import React from 'react';
import i18n from '../i18n';
import { useTranslation } from 'react-i18next';
export default function InlineShortOverview(){
    const [t, i18n] = useTranslation();
    return (
        <>
        <br></br>
        <br></br>
    <div className="container">
        <div className="intro-area">
            <div className="row justify-content-center">
                <div className="col-lg-3 col-sm-6">
                    <div className="single-intro-wrap">
                        <div className="thumb">
                            <img src="assets/img/intro/1.png" alt="img" />
                        </div>
                        <div className="wrap-details">
                            <h6><a href="#">{t('130,000 online courses.1')}</a></h6>
                            <p>{t('Enjoy a variety of fresh topics.1')}</p>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-sm-6">
                    <div className="single-intro-wrap">
                        <div className="thumb">
                            <img src="assets/img/intro/2.png" alt="img" />
                        </div>
                        <div className="wrap-details">
                            <h6><a href="#">{t('Expert instruction.1')}</a></h6>
                            <p>{t('Enjoy a variety of fresh topics.1')}</p>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-sm-6">
                    <div className="single-intro-wrap">
                        <div className="thumb">
                            <img src="assets/img/intro/3.png" alt="img" />
                        </div>
                        <div className="wrap-details">
                            <h6><a href="#">{t('Lifetime access.1')}</a></h6>
                            <p>{t('Learn on your schedule.1')}</p>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-sm-6">
                    <div className="single-intro-wrap">
                        <div className="thumb">
                            <img src="assets/img/intro/1.png" alt="img" />
                        </div>
                        <div className="wrap-details">
                            <h6><a href="#">{t('130,000 online courses.1')}</a></h6>
                            <p>{t('Enjoy a variety of fresh topics.1')}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </>
    )
}