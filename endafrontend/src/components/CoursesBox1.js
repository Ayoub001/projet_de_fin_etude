import {Link, useParams} from 'react-router-dom';
import {useState, useEffect} from 'react'; 
import axios from 'axios';
import React from 'react';

const baseUrl='http://127.0.0.1:8000/api';
function CoursesBox1(){
const [popularcourseData, setpopularcourseData]=useState([]);
const [teacherData, setteacherData]=useState([]);
const {category_id, category_slug}=useParams();
const [categoryData, setcategoryData]=useState([]);
let {course_id}=useParams();
useEffect(() => {
// fetch popular courses
try{
axios.get(baseUrl+'/popular-courses/?popular=1')
.then((res) => {
setpopularcourseData(res.data);
});
}catch(error) {
    console.log(error);
}
try{
axios.get(baseUrl+'/course/'+7)
.then((res)=>{
setteacherData(res.data.teacher);
});
}catch(error) {
    console.log(error);
}
try{
axios.get(baseUrl+'/category')
.then((res)=>{
    setcategoryData(res.data);
    });
    }catch(error) {
        console.log(error);
    }



},[]); 
    return(
        
       <> 
<div className="row mb-4">
{popularcourseData && popularcourseData.map((row, index)=>
<div className="col-md-3">
<div className="card" >
<Link to={`/detail/${row.course.id}`}><img src={row.course.featured_img} className="card-img-top" alt={row.course.title} /></Link> 
<div className="card-body" >
            <div className="wrap-details">
            <h6> <Link to={`/detail/${row.course.id}`}>{row.course.title} </Link></h6>`
                <div className="user-area">
                    <div className="user-details">
                        <img src="assets/img/author/1.png" alt="img" />
                        <Link to={`/teacher-detail/${teacherData.id}`}>{teacherData.full_name}</Link>
                    </div>
                    <div className="user-rating">
                        <span><i className="fa fa-star"></i>{row.rating}</span>({row.course.course_views})
                    </div>
                </div>
                <div className="price-wrap">
                    <div className="row align-items-center">
                        <div className="col-6">
                            <a href="#">Development</a>
                        </div>
                        <div className="col-6 text-end">
                            <div className="price">$30</div>
                        </div>
                    </div>
                </div>
                </div>
</div>
</div>
</div>
)}
</div>
        </>
       
 
    )
}
export default CoursesBox1;