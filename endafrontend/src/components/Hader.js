
import {Link} from 'react-router-dom'; 
import {useState} from 'react';
import React from 'react'
import { useTranslation } from 'react-i18next';
import i18n from '../i18n';
function Header() {
const [searchString, setsearchString]=useState({
   'search': ''
});
const teacherLoginStatus=localStorage.getItem('teacherLoginStatus'); 
const studentLoginStatus=localStorage.getItem('studentLoginStatus');
const handleChange=(event)=>{
setsearchString({
  ...searchString,
[event.target.name]: event.target.value
});

}
const searchCourse=() =>{
if(searchString.search!==''){
window.location.href='/search/'+searchString.search
}
}


let menu1;
let menu;

if (studentLoginStatus==='true') {
  menu = (

    <ul className="sub-menu">
      <li>
      <Link to="/user-dashboard">Dashboard</Link>
      </li>
      <li>
      <Link to="/user-logout">Logout</Link>
      </li>
    </ul>
        
  );
}
else {
  menu = (
    <ul className="sub-menu">
      <li>
      <Link to="/user-login">Login</Link>
      </li>
      <li>
      <Link to="/user-register">Register</Link>
      </li>
    </ul>
  );
}
if (teacherLoginStatus==='true') {
  menu1 = (

    <ul className="sub-menu">
      <li>
      <Link to="/teacher-dashboard">Dashboard</Link>
      </li>
      <li>
      <Link to="/teacher-logout">Logout</Link>
      </li>
    </ul>
        
  );
}
else {
  menu1 = (
    <ul className="sub-menu">
      <li>
      <Link to="/teacher-login">Login</Link>
      </li>
      <li>
      <Link to="/teacher-register">Register</Link>
      </li>
    </ul>
  );
}
const [t, i18n] = useTranslation();
function handleClick(lang) { i18n.changeLanguage(lang);
}

return (
  <>
  
  <header className="navbar-area">
    <nav className="navbar navbar-expand-lg">
      <div className="container nav-container">
        <div className="responsive-mobile-menu">
          <button
            className="menu toggle-btn d-block d-lg-none"
            datatarget="#themefie_main_menu"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="icon-left"></span>
            <span className="icon-right"></span>
          </button>
        </div>
        <div className="logo">
          <Link href="/">
          <a className="main-logo">
            <img src="/assets/img/logo.png" alt="img" />
          </a>
          </Link>
        </div>
        <div className="collapse navbar-collapse" id="themefie_main_menu">
          <ul className="navbar-nav menu-open text-end">
            <li className="current-menu-item">
              <Link href="/">
              <form className="d-flex">
                  <input name="search" onChange={handleChange} className="form-control me-2" type="search" placeholder={t('Search by course title.1')} aria-label="Search" style={{height: "2.5em", marginTop:"1em"}} />
                  <button onClick={searchCourse} className="btn btn-warning" type="button" style={{width: "6em"}} >{t('Search.1')}</button>
              </form>
              </Link>
            </li>
            </ul>
            </div>
        
        <div className="nav-right-part nav-right-part-mobile">
          <ul>
            <li>
              <a className="" href="#">
                <i className="fa fa-shopping-basket"></i>
              </a>
            </li>
          </ul>
        </div>
        <div className="collapse navbar-collapse" id="themefie_main_menu">
          <ul className="navbar-nav menu-open text-end">
            <li className="current-menu-item">
            <Link className="nav-link active" aria-current="page" to="/">{t('Home.1')}</Link>
            </li>

            <li className="menu-item-has-children">
            <Link to="/all-courses">{t('Courses.1')}</Link>
              <ul className="sub-menu" id="demo">
                
              </ul>
            </li>
            <li className="current-menu-item">
            <Link to="/category">{t('Categories.1')}</Link>
            </li>
            <li className="current-menu-item">
            <Link to="/contact-us">{t('Contact-Us.1')}</Link>
            </li>
            <li className="current-menu-item">
              <Link href="/about">
              <Link to="/about-us">{t('About.1')}</Link>
              </Link>
            </li>

            <li className="menu-item-has-children">
              <a className="">
                <i className="fa fa-users"></i>
              </a>
              {menu}
            </li>
            <li className="menu-item-has-children">
              <a className="">
              <i className="fas fa-chalkboard-teacher"></i>
              </a>
              {menu1}
            </li>
            <li>
                            <button className="btn btn-light" data-mdb-ripple-color="dark" onClick={() =>handleClick('en')}>
                            English
                            </button>
                            <button class="btn btn-dark" onClick={() =>handleClick('ar')}>
                            Arabic
                            </button>
            </li>
            <li className="current-menu-item">
              <a href=" ">  </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>

    <br></br>
        <br></br>
        <br></br>
</>
)
}
export default Header;
