
import {useState, useEffect} from 'react'; 
import axios from 'axios';
import React from 'react'
import i18n from '../i18n';
import { useTranslation } from 'react-i18next';
const baseUrl='http://127.0.0.1:8000/api/contact/'; 
function ContactUs () {
const [ContactData, setContactData]=useState({ 
    'full_name': '',
    'email': '',
    'query_txt':'',
    'status':''
});
// Change Element value
const handleChange=(event)=>{
setContactData({
...ContactData,
[event.target.name]: event.target.value
});
}
// End
// Submit Form
const submitForm=()=>{
const contactFormData=new FormData();
contactFormData.append("full_name", ContactData.full_name) 
contactFormData.append("email", ContactData.email) 
contactFormData.append("query_txt", ContactData.query_txt)
try{
axios.post(baseUrl, contactFormData).then((response)=>{ setContactData({
'full_name': '',
'email': '',
'query_txt': '',
'status': 'success'
});
});
}catch(error) {
console.log(error);
setContactData({'status': 'error'})
}
};
// End
const listStyle={
'list-style': 'none' 
}
useEffect (() =>{
document.title="Contact Us"
});
useEffect(() => {
    window.scrollTo(0, 0)
  }, [])
  const [t, i18n] = useTranslation();
return (

<div className="container mt-4">
<div className="row">
<div className="col-7">
{ContactData.status=='success' && <p className="text-success">{t('Thanks for your Contacting Us.1')}</p>} 
{ContactData.status == 'error' && <p className="text-danger">{t('Something wrong happened!!.1')}</p>} 
<div className="card">
<h5 className="card-header">{t('Contact Us.1')}</h5>
<div className="card-body">
{/* <form> */}
<div className="mb-3">
<label for="exampleInputEmail1" className="form-label">{t('Full Name.1')}</label>
<input value={ContactData.full_name} onChange={handleChange} name="full_name" type="text" className="form-control" />
</div>
<div className="mb-3">
<label for="exampleInputEmail1" className="form-label">{t('Email.1')}</label>
<input value={ContactData.email} onChange={handleChange} name="email" type="email" className="form-control" />
</div>
<div className="mb-3">
<label for="exampleInputEmail1" className="form-label">{t('Query.1')}</label>
</div>
<textarea rows="10" value={ContactData.query_txt} onChange={handleChange} name="query_txt" className="form-control"></textarea> <button onClick={submitForm} type="submit" className="btn btn-primary">{t('Send.1')}</button>
{/* </form> */}
</div>
</div>
</div>
<div className="col-4 offset-1">
<h3 className='border-bottom'>{t('Address.1')}</h3>
<ul style={listStyle}>
<li>
<label className='fw-bold'>{t('Address.1')}</label>
<span className="ms-2">..........................</span>
</li>
<li>
<label className='fw-bold'>{t('Mobile.1')}</label> 
<span className="ms-2">..........................</span>
</li>
</ul>
</div>
</div>
</div>


)
}
export default ContactUs;