import {Link} from 'react-router-dom';
import {useState, useEffect} from 'react'; 
import axios from 'axios';
import { useTranslation } from 'react-i18next';
import i18n from '../i18n';
import React from 'react';
const baseUrl='http://127.0.0.1:8000/api';
function CoursesBox(){
const [teacherData, setteacherData]=useState([]);

const [courseData, setCourseData]=useState([]); 
const [popularcourseData, setpopularcourseData]=useState([]);

 const [testimonialData, settestimonialData]=useState([]); 
 // Fetch courses when page load
useEffect(() => {
// Fetch 4 courses
try{
axios.get(baseUrl+'/course/?result=4')
.then((res)=>{
setCourseData(res.data.results);
});
}catch(error) {
    console.log(error);
}


// fetch popular courses
try{
axios.get(baseUrl+'/popular-courses/?popular=1')
.then((res) => {
setpopularcourseData(res.data);
});
}catch(error) {
    console.log(error);
}

try{
    axios.get(baseUrl+'/course/'+7)
    .then((res)=>{
    setteacherData(res.data.teacher);
    });
    }catch(error) {
        console.log(error);
    }

// fetch student testimonial
try{
axios.get(baseUrl+'/student-testimonial/')
.then((res)=>{
settestimonialData(res.data);
});
}catch(error) {
    console.log(error);
}




},[]); 


const [t, i18n] = useTranslation();
const tdata = courseData.map((item) => {
  if(i18n.language === 'ar') {
      
      return ({
        'title': item.title_ar,
        'description': item.description_ar,
        'featured_img': item.featured_img,
        'techs': item.techs,
        });
      
  }
  return ({
    'title': item.title,
    'description': item.description,
    'featured_img': item.featured_img,
    'techs': item.techs,

  })

});


    return(
        
       <> 
<div className="row mb-4">
{tdata ? tdata.map((item) => (
<div className="col-md-3">
<div className="card" >
<Link to={`/detail/${item.id}`}><img src={item.featured_img} className="card-img-top" alt={item.title} /></Link> 
<div className="card-body" >
            <div className="wrap-details">
            <h6> <Link to={`/detail/${item.id}`}>{item.title}</Link></h6>`
                <div className="user-area">
                    <div className="user-details">
                        <img src="assets/img/author/1.png" alt="img" />
                        <Link to={`/teacher-detail/${teacherData.id}`}>{teacherData.full_name}</Link>
                    </div>
                    <div className="user-rating">
                    <span><i className="fa fa-star"></i>{item.rating}</span>({item.course_views})
                    </div>
                </div>
                <div className="price-wrap">
                    <div className="row align-items-center">
                        <div className="col-6">
                            <a href="#">Development</a>
                        </div>
                        <div className="col-6 text-end">
                            <div className="price">$30</div>
                        </div>
                    </div>
                </div>
                </div>
</div>
</div>
</div>
)) : <p>no post yet!</p>}
</div>
        </>
       
 
    )
}
export default CoursesBox;