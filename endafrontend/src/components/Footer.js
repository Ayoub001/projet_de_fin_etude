
import {Link} from 'react-router-dom';
import {useState, useEffect} from 'react'; 
import axios from 'axios';
import React from 'react'
import { useTranslation } from 'react-i18next';
import i18n from '../i18n';
const baseUrl='http://127.0.0.1:8000/api'; 
function Footer() {
const [pagesData, setpagesData]=useState([]); // Fetch courses when page load
useEffect (() =>{
try{
axios.get(baseUrl+'/pages/')
.then((res)=>{
  setpagesData(res.data);
});

}catch(error) {
console.log(error);
}
},[]);
const [t, i18n] = useTranslation();
return(
  <>
  <footer className="footer-area">
  <div className="footer-inner">
      <div className="container">
          <div className="row">
              <div className="col-lg-8 col-md-4 col-sm-6">
                  <div className="footer-widget widget widget_link">
                      <h4 className="widget-title">{t('Categories.1')}</h4>
                      <div className="row">
                          <div className="col-lg-4">

                            <header className="App-header">

                            <p>{t('Marketing.1')}</p>
                            <p>{t('Design.1')}</p>
                            </header>
                              <ul className="pe-5">
                                  <li><a href="category.html">{t('Development.1')}</a></li>
                                  <li><a href="category.html">{t('Business.1')}</a></li>
                                  <li><a href="category.html">{t('Finance & Accounting.1')}</a></li>
                                  <li><a href="category.html">{t('IT & Software.1')}</a></li>
                                  <li><a href="category.html">{t('Office Productivity.1')}</a></li>
                                  <li><a href="category.html">{t('Design.1')}</a></li>
                                  <li><a href="category.html">{t('Marketing.1')}</a></li>
                              </ul>
                          </div>
                          <div className="col-lg-4">
                            
                              <ul className="pe-5">
                                  <li><a href="category.html">{t('Lifiestyle.1')}</a></li>
                                  <li><a href="category.html">{t('Photography & Video.1')}</a></li>
                                  <li><a href="category.html">{t('Health & Fitness.1')}</a></li>
                                  <li><a href="category.html">{t('Music.1')}</a></li>
                                  <li><a href="category.html">{t('UX Design.1')}</a></li>
                                  <li><a href="category.html">{t('Seo.1')}</a></li>
                                  <li><a href="category.html">{t('Business Analysis and Strategy.1')}</a></li>
                              </ul>
                          </div>
                          <div className="col-lg-4">
                              <ul>
                                  <li><a href="category.html">{t('Customer Service.1')}</a></li>
                                  <li><a href="category.html">{t('Human Resources.1')}</a></li>
                                  <li><a href="category.html">{t('Leadership and Management.1')}</a></li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>
              <div className="col-lg-2 col-md-4 col-sm-6">
                  <div className="footer-widget widget widget_link">
                      <h4 className="widget-title">{t('Link.1')}</h4>
                      <ul className="pe-4">
                          <li><a href="blog.html">{t('News & Blogs.1')}
                              </a></li>
                          <li><a href="blog-cat.html">{t('Blog Category.1')}</a></li>
                          <li><a href="blog-details.html">{t('Blog Details.1')}</a></li>
                          <li><a href="course.html">{t('Course.1')}</a></li>
                          <li><a href="course-details.html">{t('Course Details.1')}</a></li>
                          <li><a href="instructor.html">{t('Instructor.1')}</a></li>
                          <li><a href="instructor-details.html">{t('Instructor Details.1')}</a></li>
                      </ul>
                  </div>
              </div>
              <div className="col-lg-2 col-md-4 col-sm-6">
                  <div className="footer-widget widget widget_link">
                      <h4 className="widget-title">{t('Support.1')}</h4>
                      <ul className="pe-4">
                          <li><a href="index.html">{t('Documentation.1')}</a></li>
                          <li><a href="faq.html">{t('FAQS.1')}</a></li>
                          <li><a href="dashboard.html">{t('Dashboard.1')}</a></li>
                          <li><a href="contact.html">{t('Contact.1')}</a></li>
                      </ul>
                  </div>
              </div>
          </div>
      </div>
  </div>
  
  <div className="container">
      <div className="footer-bottom">
          <div className="row">
              <div className="col-xl-7 align-self-center">
                  <div className="d-md-flex align-items-center mb-4 mb-xl-0">
                      <div className="logo d-inline-block">
                         <img src="assets/img/logo.png" alt="img" />
                      </div>
                      <div className="copyright-area">
                          <p>© 2021 - 2023</p>
                      </div>
                  </div>
              </div>
              <div className="col-xl-5 align-self-center text-xl-end">
                  <ul className="social-area d-inline-block">
                      <li><a className="active" href="#"><i className="fab fa-facebook-f"></i></a></li>
                      <li><a href="#"><i className="fab fa-twitter"></i></a></li>
                      <li><a href="#"><i className="fab fa-linkedin-in"></i></a></li>
                      <li><a href="#"><i className="fab fa-instagram"></i></a></li>
                      <li><a href="#"><i className="fab fa-dribbble"></i></a></li>
                      <li><a href="#"><i className="fab fa-behance"></i></a></li>
                  </ul>
              </div>
          </div>
      </div>
  </div>  
</footer>

<div className="back-to-top">
  <span className="back-top"><i className="fas fa-angle-double-up"></i></span>
</div>    
</>
);
}
export default Footer;