
import Hader from './Hader'
import Home from './Home'
import About from './About'
import Footer from './Footer'
import Login from './User/Login'
import {Routes as Switch,Route} from 'react-router-dom';
import React from "react";
import RecommendedCourses from './User/RecommendedCourses'
import ProfileSetting from './User/ProfileSetting'
import AddCourse from './Teacher/AddCourse'
import MyUsers from './Teacher/MyUsers'
import TeacherDashboard from './Teacher/TeacherDashboard'
import TeacherProfileSetting from './Teacher/TeacherProfileSetting'
import TeacherChangePassword from './Teacher/TeacherForgotChangePassword'
import TeacherDetail from './Teacher/TeacherDetail'
import AllCourses from './AllCourses'
import CategoryCourses from './CategoryCourses'
import Search from './Search'
import Logout from './User/Logout'



import Category from './Category'
import ForgotChangePassword from './User/ChangePassword'
import VerifyStudent from './User/VerifyStudent'
import Dashboard from './User/Dashboard'
import MyCourses from './User/MyCourses'
import FavoriteCourses from './User/FavoriteCourses'
import ChangePassword from './User/ChangePassword'
import TeacherRegister from './Teacher/TeacherRegister'
import TeacherLogin from './Teacher/TeacherLogin'
import TeacherLogout from './Teacher/TeacherLogout'
import VerifyTeacher from './Teacher/VerifyTeacher'
import CourseDetaile from './CourseDetaile'
import ForgotPassword from './Teacher/TeacherForgotPassword'
import TeacherMyCourses from './Teacher/TeacherMyCourses'
import TeacherCourses from './Teacher/TeacherCourses'
import AddChapter from './Teacher/AddChapter'
import EditCourse from './Teacher/EditCourse'
import AddAssignment from './Teacher/AddAssignment'
import StudentAssignments from './User/StudentAssignments'
import Header from './Hader'
import FAQ from './FAQ'
import Page from './Page'
import ContactUs from './ContactUs'
import AllQuiz from './Teacher/AllQuiz'
import AssignQuiz from './Teacher/AssignQuiz'
import AddQuizQuestion from './Teacher/AddQuizQuestion'
import AddQuiz from './Teacher/AddQuiz'
import EditChapter from './Teacher/EditChapter'
import CheckQuizinCourse from './Teacher/CheckQuizinCourse'
import TakeQuiz from './User/TakeQuiz'
import CourseQuizList from './User/CourseQuizList'
import UserList from './Teacher/UserList'
import MessageList from './Teacher/MessageList'
import AddStudyMaterial from './Teacher/AddStudyMaterial'
import StudyMaterials from './Teacher/StudyMaterials'
import PolpularCourses from './PolpularCourses'
import PopularTeacher from './PopularTeacher'
import AttemptedStudents from './Teacher/AttemptedStudents'
import CourseChapters from './Teacher/CourseChapters'
import TeacherSkillCourses from './Teacher/TeacherSkillCourses'
import UserStudyMaterials from './User/UserStudyMaterials'
import QuizQuestions from './Teacher/QuizQuestions'
import EditQuiz from './Teacher/EditQuiz'
import EnrolledStudents from './Teacher/EnrolledStudents'
import ShowAssignment from './Teacher/ShowAssignment'
import Register from './User/Register'
import UserForgotPassword from './User/UserForgotPassword'
import MyTeachers from './User/MyTeachers'
import Video from './Video'


function Main() {
    return (
      <div className="App">
<Hader /> 
<Switch>
<Route path="/" element={<Home />} />
<Route path="/about-us" element={<About />} />
<Route path="/video" element={<Video />} />
<Route path="/detail/:course_id" element={<CourseDetaile />} /> 
<Route path="/search/:searchstring" element = {<Search />} />
<Route path="/user-login" element={<Login />} />
<Route path="//my-teachers" element={<MyTeachers />} />
<Route path="/teacher-forgot-password" element={<ForgotPassword/>} />
<Route path="/teacher-change-password/:teacher_id" element={<ForgotChangePassword />}/>
<Route path="/user-logout" element={<Logout/>} />
<Route path="/user-forgot-password" element={<UserForgotPassword/>} />
<Route path="/user-register" element={<Register />} />
<Route path="/verify-student/:student_id" element={<VerifyStudent />} />
<Route path="/user-dashboard" element={<Dashboard />} />
<Route path="/my-courses" element={<MyCourses />} />
<Route path="/favorite-courses" element={<FavoriteCourses />} />
<Route path="/recommended-courses" element={<RecommendedCourses />} /> 
<Route path="/profile-setting" element={<ProfileSetting />} />
<Route path="/change-password" element={<ChangePassword />} /> 
<Route path="/teacher-login" element={<TeacherLogin />} />
<Route path="/teacher-logout" element={<TeacherLogout />} />
<Route path="/teacher-register" element={<TeacherRegister />} />
<Route path="/verify-teacher/:teacher_id" element={<VerifyTeacher />} /> 
<Route path="/teacher-dashboard" element={<TeacherDashboard />} />
<Route path="/teacher-courses" element={<TeacherCourses />} />
<Route path="/enrolled-students/:course_id" element={<EnrolledStudents />} />
<Route path="/add-courses" element={<AddCourse />} />
<Route path="/teacher-my-courses" element={<TeacherMyCourses />} />
<Route path="/edit-course/:course_id" element={<EditCourse />} /> 
<Route path="//my-users" element={<MyUsers />} /> 
<Route path="/add-chapter/:course_id" element={<AddChapter />} />
<Route path="/add-assignment/:student_id/:teacher_id" element={<AddAssignment />} /> 
<Route path="/show-assignment/:student_id/:teacher_id" element={<ShowAssignment />} /> 
<Route path="/my-assignments/" element={<StudentAssignments />} />
<Route path="/quiz" element={<AllQuiz />} />
<Route path="/add-quiz" element={<AddQuiz />} />
<Route path="/edit-quiz/:quiz_id" element={<EditQuiz />} />

<Route path="/all-questions/:quiz_id" element={<QuizQuestions />} /> 
<Route path="/add-quiz-question/:quiz_id" element={<AddQuizQuestion />} /> 
<Route path="/assign-quiz/:course_id" element={<AssignQuiz />} />
<Route path="/attempted-students/:quiz_id" element={<AttemptedStudents />} />
<Route path="/course-quiz/:course_id" element={<CourseQuizList />} /> 
<Route path="/take-quiz/:quiz_id" element={<TakeQuiz />} />
<Route path="/user/study-materials/:course_id" element={<UserStudyMaterials />} /> 
<Route path="/study-materials/:course_id" element={<StudyMaterials />} /> 
<Route path="/add-study/:course_id" element={<AddStudyMaterial />} />
<Route path="/teacher-users" element={<UserList />} />
<Route path="/teacher-profile-setting" element={<TeacherProfileSetting />} />
<Route path="/teacher-change-password" element={<TeacherChangePassword />} />
<Route path="/teacher-detail/:teacher_id" element={<TeacherDetail />} /> 
<Route path="/all-courses" element={<AllCourses />} />
<Route path="/all-chapters/:course_id" element ={<CourseChapters />} /> 
<Route path="/edit-chapter/:chapter_id" element={<EditChapter />} /> 
<Route path="/popular-courses" element={<PolpularCourses />} />
<Route path="/popular-teachers" element={<PopularTeacher />} />
<Route path="/category" element={<Category />} />
<Route path="/course/:category_id/:category_slug" element={<CategoryCourses />} />
<Route path="/teacher-skill-courses/:skill_name/:teacher_id" element={<TeacherSkillCourses />} /> 
<Route path="/faq" element={<FAQ />} />
<Route path="/page/:page_id/:page_slug" element={<Page />} />
<Route path="/contact-us" element={<ContactUs />} />
      
          

          </Switch>
          <Footer/>

      </div>
      

    );
  }
  
  export default Main