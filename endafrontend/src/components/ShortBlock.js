import React from 'react';
import i18n from '../i18n';
import { useTranslation } from 'react-i18next';
function ShortBlock(){
    const [t, i18n] = useTranslation();
    return (
        <div className="item">
        <div className="single-service-wrap">
            <h6>{t('Digital Marketing.1')}</h6>
            <p>{t('236 Course Available.1')}</p>
        </div>
    </div>
    )
}
export default ShortBlock;