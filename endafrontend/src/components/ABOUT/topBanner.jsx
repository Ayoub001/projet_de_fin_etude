import React from 'react'
import i18n from '../../i18n';
import { useTranslation } from 'react-i18next';
export default function TopBanner(){
    const [t, i18n] = useTranslation();
    return(
        <>
        <section className="banner-area instructor-banner p-0" style={{backgroundColor: '#11142D'}}>
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-5 col-8">
                    <div className="thumb">
                       <img src="/assets/img/instructor.png" alt="img"  />
                    </div>
                </div>
                <div className="col-md-7 col-sm-10 align-self-center">
                    <div className="banner-inner text-md-start text-center">
                        <h1 className="text-white">{t('Make a global impact.1')}</h1>
                        <div className="banner-content me-0">
                            <p className="text-white">{t('Create an online video course and earn money by teaching people around the world..1')}</p>                         
                        </div>
                        <a className="btn btn-base" href="instructor-details.html">{t('the best instructors.1')}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
        </>
    )
}