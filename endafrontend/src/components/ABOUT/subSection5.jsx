
import React from 'react'
import i18n from '../../i18n';
import { useTranslation } from 'react-i18next';
export default function SubSection5(){
  const [t, i18n] = useTranslation();
    return(
        <>
        <br></br>
        <br></br>
        <br></br>
        <div className="container text-center">
        <div className="row justify-content-center">
                <div className="col-lg-8">
                    <div className="section-title">
                        <h2>{t('Meet My Team.1')}</h2>
                        <p>{t('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget aenean accumsan bibendum gravida maecenas augue elementum et neque. Suspendisse imperdiet ..1')}</p>
                        <br></br>
                    </div>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                </div>
            </div>
        </div>
        <div className="container">
          <div className="intro-area-2">
            <div className="row justify-content-center">
              <div className="col-lg-12">
                <div className="intro-slider owl-carousel">
                  <div className="item">
                    <div className="single-intro-wrap">
                      <div className="thumb">
                       <img src="/assets/img/instructor-details.png" alt="img"  />
                      </div>
                      <div className="wrap-details">
                        <h6>
                          <a href="#">{t('Digital Marketing.1')}</a>
                        </h6>
                        <p>{t('236 Course Available.1')}</p>
                      </div>
                    </div>
                  </div>
                  <div className="item">
                    <div className="single-intro-wrap">
                      <div className="thumb">
                       <img src="/assets/img/instructor-details.png" alt="img"  />
                      </div>
                      <div className="wrap-details">
                        <h6>
                          <a href="#">{t('Web Development.1')}</a>
                        </h6>
                        <p>{t('236 Course Available.1')}</p>
                      </div>
                    </div>
                  </div>
                  <div className="item">
                    <div className="single-intro-wrap">
                      <div className="thumb">
                       <img src="/assets/img/instructor-details.png" alt="img"  />
                      </div>
                      <div className="wrap-details">
                        <h6>
                          <a href="#">{t('Photography.1')}</a>
                        </h6>
                        <p>{t('236 Course Available.1')}</p>
                      </div>
                    </div>
                  </div>
                  <div className="item">
                    <div className="single-intro-wrap">
                      <div className="thumb">
                       <img src="/assets/img/instructor-details.png" alt="img"  />
                      </div>
                      <div className="wrap-details">
                        <h6>
                          <a href="#">{t('Data & Analytics.1')}</a>
                        </h6>
                        <p>{t('236 Course Available.1')}</p>
                      </div>
                    </div>
                  </div>
                  <div className="item">
                    <div className="single-intro-wrap">
                      <div className="thumb">
                       <img src="/assets/img/instructor-details.png" alt="img"  />
                      </div>
                      <div className="wrap-details">
                        <h6>
                          <a href="#">{t('Graphics Design.1')}</a>
                        </h6>
                        <p>{t('236 Course Available.1')}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    
    )
}