import React from 'react';
import i18n from '../../i18n';
import { useTranslation } from 'react-i18next';
export default function SubSection2(){
    const [t, i18n] = useTranslation();
    return(
        <>
        <div className="text-center pd-top-135 pd-bottom-115" style={{background: '#F9FAFD;'}}>
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-lg-8">
                    <div className="section-title">
                        <h2>{t('Exceptional opportunities.1')}</h2>
                        <p>{t('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget aenean accumsan bibendum gravida maecenas augue elementum et neque. Suspendisse imperdiet ..1')}</p>
                    </div>
                </div>
            </div>
            <div className="row justify-content-center">
                <div className="col-lg-3 col-sm-6">
                    <div className="single-fact-wrap">
                        <div className="fact-count">
                            <h3><span className="counter">35</span>m</h3>
                        </div>
                        <div className="wrap-details">
                            <p>{t('Students worldwide.1')}</p>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-sm-6">
                    <div className="single-fact-wrap">
                        <div className="fact-count">
                            <h3><span className="counter">65</span>+</h3>
                        </div>
                        <div className="wrap-details">
                            <p>{t('Different languages.1')}</p>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-sm-6">
                    <div className="single-fact-wrap">
                        <div className="fact-count">
                            <h3><span className="counter">400</span>m</h3>
                        </div>
                        <div className="wrap-details">
                            <p>{t('Course enrollments.1')}</p>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-sm-6">
                    <div className="single-fact-wrap">
                        <div className="fact-count">
                            <h3><span className="counter">180</span>+</h3>
                        </div>
                        <div className="wrap-details">
                            <p>{t('Countries taught.1')}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>            
    </div>
        </>
        )
}