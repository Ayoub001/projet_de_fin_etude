
import React from 'react'
import i18n from '../../i18n';
import { useTranslation } from 'react-i18next';
export default function SubSection1(){
    const [t, i18n] = useTranslation();
    return(
        <>
<div className="text-center pd-top-5 pd-bottom-115">
<div className="container">
    <div className="row justify-content-center">
        <div className="col-lg-8">
            <div className="section-title">
                <h2>{t('Discover your potential.1')}</h2>
                <p>{t('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget aenean accumsan bibendum gravida maecenas augue elementum et neque. Suspendisse imperdiet ..1')}</p>
            </div>
        </div>
    </div>
    <div className="row justify-content-center">
        <div className="col-lg-4 col-sm-6">
            <div className="single-intro-wrap-2">
                <div className="thumb">
                   <img src="/assets/img/intro/01.png" alt="img"  />
                </div>
                <div className="wrap-details">
                    <h4><a href="#">{t('Earn money.1')}</a></h4>
                    <p>{t('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dui praesent nam fermentum, est neque, dignissim. Phasellus feugiat elit vulputate convallis..1')}</p>
                </div>
            </div>
        </div>
        <div className="col-lg-4 col-sm-6">
            <div className="single-intro-wrap-2">
                <div className="thumb">
                   <img src="/assets/img/intro/02.png" alt="img" />
                </div>
                <div className="wrap-details">
                    <h4><a href="#">{t('Inspire students.1')}</a></h4>
                    <p>{t('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dui praesent nam fermentum, est neque, dignissim. Phasellus feugiat elit vulputate convallis..1')}</p>
                </div>
            </div>
        </div>
        <div className="col-lg-4 col-sm-6">
            <div className="single-intro-wrap-2">
                <div className="thumb">
                   <img src="/assets/img/intro/03.png" alt="img" />
                </div>
                <div className="wrap-details">
                    <h4><a href="#">{t('Join our community.1')}</a></h4>
                    <p>{t('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dui praesent nam fermentum, est neque, dignissim. Phasellus feugiat elit vulputate convallis..1')}</p>
                </div>
            </div>
        </div>
    </div>
</div>            
</div>
</>
)
}