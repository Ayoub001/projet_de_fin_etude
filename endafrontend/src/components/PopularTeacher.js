import {Link}from 'react-router-dom'
import React from 'react'

function PopularTeacher() {
    return (
<div className='container'>
        <h3 class="pb-1 mb-4">Popular Courses</h3>
        <div class="row">
         
        <div class="col-md-3">
        <div class="card">
        <a href ="#"><img class="card-img-top" src="images02.jpg" /></a>
 <div class="card-body">
   <h5 class="card-title"><Link to ="/detail/1">Coures title</Link></h5>
 </div>
 <div class="card-footer">
 <div class="title"><span> Rating: 4.5/5 </span> <span className='float-end'> Views: 521 </span></div>
</div>
</div>
         </div>
         <div class="col-md-3">
        <div class="card">
        <a href ="#"><img class="card-img-top" src="images02.jpg" /></a>
 <div class="card-body">
   <h5 class="card-title"><a href ="#">Coures title</a></h5>
 </div>
 <div class="card-footer">
 <div class="title"><span> Rating: 4.5/5 </span> <span className='float-end'> Views: 521 </span></div>
</div>
</div>
         </div>
         <div class="col-md-3">
        <div class="card">
        <a href ="#"><img class="card-img-top" src="images02.jpg" /></a>
 <div class="card-body">
   <h5 class="card-title"><a href ="#">Coures title</a></h5>
 </div>
 <div class="card-footer">
 <div class="title"><span> Rating: 4.5/5 </span> <span className='float-end'> Views: 521 </span></div>
</div>
</div>
         </div>
         <div class="col-md-3">
        <div class="card">
        <a href ="#"><img class="card-img-top" src="images02.jpg" /></a>
 <div class="card-body">
   <h5 class="card-title"><a href ="#">Coures title</a></h5>
 </div>
 <div class="card-footer">
 <div class="title"><span> Rating: 4.5/5 </span> <span className='float-end'> Views: 521 </span></div>
</div>
</div>
         </div>





         </div>
         <div class="row mt-4 mb-5">
         
         <div class="col-md-3">
         <div class="card">
         <a href ="#"><img class="card-img-top" src="images02.jpg" /></a>
  <div class="card-body">
    <h5 class="card-title"><Link to ="/detail/1">Coures title</Link></h5>
  </div>
  <div class="card-footer">
 <div class="title"><span> Rating: 4.5/5 </span> <span className='float-end'> Views: 521 </span></div>
</div>
 </div>
          </div>
          <div class="col-md-3">
         <div class="card">
         <a href ="#"><img class="card-img-top" src="images02.jpg" /></a>
  <div class="card-body">
    <h5 class="card-title"><a href ="#">Coures title</a></h5>
  </div>
  <div class="card-footer">
 <div class="title"><span> Rating: 4.5/5 </span> <span className='float-end'> Views: 521 </span></div>
</div>
 </div>
          </div>
          <div class="col-md-3">
         <div class="card">
         <a href ="#"><img class="card-img-top" src="images02.jpg" /></a>
  <div class="card-body">
    <h5 class="card-title"><a href ="#">Coures title</a></h5>
  </div>
  <div class="card-footer">
 <div class="title"><span> Rating: 4.5/5 </span> <span className='float-end'> Views: 521 </span></div>
</div>
 </div>
          </div>
          <div class="col-md-3">
         <div class="card">
         <a href ="#"><img class="card-img-top" src="images02.jpg" /></a>
  <div class="card-body">
    <h5 class="card-title"><a href ="#">Coures title</a></h5>
  </div>
  <div class="card-footer">
 <div class="title"><span> Rating: 4.5/5 </span> <span className='float-end'> Views: 521 </span></div>
</div>
 </div>
          </div>
 
 
 
 
 
          </div>
         <nav aria-label="Page navigation example">
<ul class="pagination justify-content-center">
<li class="page-item"><a class="page-link" href="#">Previous</a></li>
<li class="page-item"><a class="page-link" href="#">1</a></li>
<li class="page-item"><a class="page-link" href="#">2</a></li>
<li class="page-item"><a class="page-link" href="#">3</a></li>
<li class="page-item"><a class="page-link" href="#">Next</a></li>
</ul>
</nav>
</div>
    );
}
export default PopularTeacher;