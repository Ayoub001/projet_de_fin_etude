import React from 'react';
function TestimonialBlock(){
    return(
        <>
        <br></br>
        <div className="item">
            <div className="single-testimonial-wrap">
                <div className="thumb">
                    <img src="assets/img/quote.png" alt="img" style={{width: "2em"}} />
                </div>
                <div className="wrap-details">
                    <h5><a href="#">Super fast WordPress themes</a></h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Velit placerat sit
                        feugiat ornare tortor arcu, euismod pellentesque porta. Lacus, semper congue
                        consequat, potenti suspendisse luctus cras vel.</p>
                    <span>- Jessica Jessy</span>
                     
                </div>
            </div>
        </div>
        </>
    )
}export default TestimonialBlock;