﻿
import {useEffect, useState} from 'react';
import {useNavigate} from 'react-router-dom';
import axios from 'axios';
import React from 'react'

const baseUrl='http://127.0.0.1:8000/api/teacher/'; 
function TeacherRegister() {
const navigate=useNavigate();
const [teacherData, setteacherData]=useState({ 
    'full_name': '',
    'email': '', 'password': '', 
    'qualification':'',
    'mobile_no': '',
    'skills': '',
    'status': '',
    'otp_digit': '',
});

// Change Element value
const handleChange=(event)=>{
setteacherData({
teacherData,
[event.target.name]: event.target.value
});
}
// End
// Submit Form
const submitForm=()=>{
    const otp_digit=Math.floor(100000+ Math.random() * 900000); 
    const teacherFormData=new FormData();
    teacherFormData.append("full_name", teacherData.full_name) 
    teacherFormData.append("email", teacherData.email)
    teacherFormData.append("password", teacherData.password)
    teacherFormData.append("qualification", teacherData.qualification)
    teacherFormData.append("mobile_no", teacherData.mobile_no) 
    teacherFormData.append("skills", teacherData.skills) 
    teacherFormData.append("otp_digit", otp_digit)
try{
axios.post(baseUrl, teacherFormData).then((response)=>{
    console.log(response.data);
navigate('/verify-teacher/' + response.data.id);
});

}catch(error) {
console.log(error);
setteacherData({'status':'error'})
}
};
// End
useEffect (() =>{
document.title="Teacher Register"
});

return (
<div className="container mt-4">
<div className="row">
<div className="col-6 offset-3">
{teacherData.status='success' && <p className="text-success">Thanks for your registeration</p>} 
{teacherData.status='error' && <p className="text-danger">Something wrong happened!!</p>}
<div className="card">
<h5 className="card-header">Teacher Register</h5>
<div className="card-body">
{/* <form> */}
<div className="mb-3">
<label for="exampleInputEmail1" className="form-label">Full Name</label>
<input value={teacherData.full_name} onChange={handleChange} name="full_name" type="text" className="form-control" />
</div>
<button type="submit" onClick={submitForm} className="btn btn-primary">Verify</button>
</div>
</div>
</div>
</div>
</div>
)
}
export default TeacherRegister;