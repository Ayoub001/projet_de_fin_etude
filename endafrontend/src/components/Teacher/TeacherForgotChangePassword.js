import {Link} from 'react-router-dom';
import TeacherSidebar from './TeacherSidebar'; 
import {useState, useEffect} from 'react'; 
import {useParams} from 'react-router-dom'; 
import axios from 'axios';
import Swal from 'sweetalert2';
import React from 'react'

const baseUrl='http://127.0.0.1:8000/api'; 
function TeacherForgotChangePassword(){
const [teacherData, setteacherData]=useState({ 
    'password' : ''
});
const teacherId=localStorage.getItem('teacherId');
const [successMsg, setsuccessMsg]=useState(''); 
const [errorMsg, seterrorMsg]=useState('');
console.log(teacherId)
const handleChange=(event)=>{
setteacherData({
...teacherData,
[event.target.name]: event.target.value
});
}
// End
// Submit Form
const submitForm=()=>{
const teacherFormData=new FormData();
teacherFormData.append("password", teacherData.password)
 try{  
    axios.post(baseUrl+'/teacher-change-password/'+teacherId+'/', teacherFormData)
    .then((res)=>{ 
        if (res.data.bool ===true) {
            setsuccessMsg (res.data.msg); 
            seterrorMsg('');
            }else{
            seterrorMsg(res.data.msg);
            setsuccessMsg('');
            }
    });
    
    }catch(error) {
   
    console.log(error);
    setteacherData({'status': 'error'})
   
    };

  
    
    const teacherLoginStatus=localStorage.getItem('teacherLoginStatus')
    if(teacherLoginStatus=='true'){
    window.location.href='/teacher-dashboard';
    }

};
    
return (
<div className="container mt-4">
<div className="row">
<div className="col-6 offset-3">
<div className="card">
<h5 className="card-header">Enter your password</h5>
<div className="card-body">
{successMsg && <p className='text-success'>{successMsg}</p>}
{errorMsg && <p className='text-danger'>{errorMsg}</p>}
<div className="mb-3">
<label for="exampleInputEmail1" className="form-label">Password</label>
<input type="text" name="password" value={teacherData.password} onChange={handleChange} class="form-control" id="inputPassword" /> 
</div>
<button type="submit" onClick={submitForm} className="btn btn-primary">Change</button>
</div>
</div>
</div>
</div>
</div>
)
}
export default TeacherForgotChangePassword;