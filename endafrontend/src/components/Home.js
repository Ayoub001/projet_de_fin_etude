import {Link} from 'react-router-dom';
import { Suspense } from 'react';
import {useState, useEffect} from 'react'; 
import axios from 'axios';
import InlineShortOverview from './InlineShortOverview';
import ShortBlock from './ShortBlock';
import CoursesBox from './CoursesBox';
import CoursesBox1 from './CoursesBox1';
import TestimonialBlock from './TestimonialBlock';
import { useTranslation } from 'react-i18next';
import i18n from '../i18n';
import React from 'react';

const baseUrl='http://127.0.0.1:8000/api';
function Home () {
  
const [courseData, setCourseData]=useState([]); 
const [popularcourseData, setpopularcourseData]=useState([]);
 const [popularteacherData, setteacherData]=useState([]); 
 const [testimonialData, settestimonialData]=useState([]); 
 // Fetch courses when page load
useEffect(() => {
// Fetch 4 courses
try{
axios.get(baseUrl+'/course/?result=4')
.then((res)=>{
setCourseData(res.data.results);
});
}catch(error) {
    console.log(error);
}


// fetch popular courses
try{
axios.get(baseUrl+'/popular-courses/?popular=1')
.then((res) => {
setpopularcourseData(res.data);
});
}catch(error) {
    console.log(error);
}



// fetch popular teachers
try{
axios.get(baseUrl+'/popular-teachers/?popular=1')
.then((res)=>{
setteacherData(res.data);
});
}catch(error) {
    console.log(error);
}
// fetch student testimonial
try{
axios.get(baseUrl+'/student-testimonial/')
.then((res)=>{
settestimonialData(res.data);
});
}catch(error) {
    console.log(error);
}

},[]); 

const [t, i18n] = useTranslation();

return (

<>
<section className="banner-area" style={{"backgroundImage":'url(assets/img/banner/0.jpg)', 'backgroundRepeat':'no-repeat',
  'backgroundSize':'auto'}}>
        <div className="container">
            <div className="row">
                <div className="col-lg-6 col-md-8 align-self-center">
                    <div className="banner-inner text-md-start text-center">
                        <h1>{t('Find the Best.1')} <span>{t('Courses.1')}</span> {t('& Upgrade.1')} <span>{t('Your Skills..1')}</span></h1>
                        <div className="banner-content">
                            <p>{t('Edufie offers professional training classes and special features to help you improve your skills..1')}</p>                         
                        </div>
                        <div className="single-input-wrap">
                            <input type="text" placeholder={t('Search your best courses.1')} />
                            <button><i className="fa fa-search"></i></button>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    <br></br>

    <br></br>
      <div className="container">
        <div className="intro-area-2">
          <div className="row justify-content-center">
            <div className="col-lg-12">
              <div className="intro-slider owl-carousel">
                <div className="item">
                  <div className="single-intro-wrap">
                    <div className="thumb">
                      <img src="assets/img/intro/001.png" alt="img" />
                    </div>
                    <div className="wrap-details">
                      <h6>
                        <a href="#">{t('Digital Marketing.1')}</a>
                      </h6>
                      <p>{t('236 Course Available.1')}</p>
                    </div>
                  </div>
                </div>
                <div className="item">
                  <div className="single-intro-wrap">
                    <div className="thumb">
                      <img src="assets/img/intro/002.png" alt="img" />
                    </div>
                    <div className="wrap-details">
                      <h6>
                        <a href="#">{t('Web Development.1')}</a>
                      </h6>
                      <p>{t('236 Course Available.1')}</p>
                    </div>
                  </div>
                </div>
                <div className="item">
                  <div className="single-intro-wrap">
                    <div className="thumb">
                      <img src="assets/img/intro/003.png" alt="img" />
                    </div>
                    <div className="wrap-details">
                      <h6>
                        <a href="#">{t('Photography.1')}</a>
                      </h6>
                      <p>{t('236 Course Available.1')}</p>
                    </div>
                  </div>
                </div>
                <div className="item">
                  <div className="single-intro-wrap">
                    <div className="thumb">
                      <img src="assets/img/intro/004.png" alt="img" />
                    </div>
                    <div className="wrap-details">
                      <h6>
                        <a href="#">{t('Data & Analytics.1')}</a>
                      </h6>
                      <p>{t('236 Course Available.1')}</p>
                    </div>
                  </div>
                </div>
                <div className="item">
                  <div className="single-intro-wrap">
                    <div className="thumb">
                      <img src="assets/img/intro/005.png" alt="img" />
                    </div>
                    <div className="wrap-details">
                      <h6>
                        <a href="#">{t('Graphics Design.1')}</a>
                      </h6>
                      <p>{t('236 Course Available.1')}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <br></br>
        <section className="trending-courses-area pd-top-5 pd-bottom-140">
        <div className="container">
            <div className="row">
                <div className="col-lg-12">
                    <div className="section-title">
                       <span><h2>{t('Most Trending Courses.1')}</h2><Link to="/all-courses" className="float-end btn btn-primary" style={{marginTop:"-2em",marginle
                    :"-10em"}}>{t('See All.1')}</Link></span>

                    </div>
                </div>
                <div className="col-lg-12">
                    <ul className="edl-nav nav nav-pills">
                        <li className="nav-item">
                            <button className="nav-link active" id="pills-1-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-1">{t('All Course.1')}</button>
                        </li>
                        <li className="nav-item">
                            <button className="nav-link" id="pills-2-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-2">Python</button>
                        </li>
                        <li className="nav-item">
                            <button className="nav-link" id="pills-3-tab" data-bs-toggle="pill" data-bs-target="#pills-3">UI
                                Design</button>
                        </li>
                        <li className="nav-item">
                            <button className="nav-link" id="pills-4-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-4">Php</button>
                        </li>
                        <li className="nav-item">
                            <button className="nav-link" id="pills-5-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-5">HTML</button>
                        </li>
                        <li className="nav-item">
                            <button className="nav-link" id="pills-6-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-6">CSS</button>
                        </li>
                    </ul>
                    <div className="tab-content">
                        <div className="tab-pane fade show active" id="pills-1">
                            <div>
                                    <CoursesBox />

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>  

    <section className="service-area">
        <div className="container">
            <div className="row">
                <div className="col-lg-4">
                    <div className="section-title">
                        <h2>{t('Find the right course.1')}</h2>
                        <p>{t('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in eget phasellus duiincidunt nascetur nisl nunc consequat. Arcu ultricies pulvinar enim vulputate..1')}</p>
                    </div>
                </div>
                <div className="col-lg-8">
                    <div className="category-service">
                        <ShortBlock />
                        <ShortBlock />
                        <ShortBlock />
                        <ShortBlock />
                        <ShortBlock />
                        <ShortBlock />
                        <ShortBlock />
                        <ShortBlock />
                        <ShortBlock />
                        <ShortBlock />
                        <ShortBlock />
                        <ShortBlock />
                        <ShortBlock />
                        <ShortBlock />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <InlineShortOverview />
    <br></br>
        <section className="trending-courses-area pd-top-5 pd-bottom-140">
        <div className="container">
            <div className="row">
                <div className="col-lg-12">
                    <div className="section-title">
                        <h2>{t('Most Trending Courses.1')}</h2>
                    </div>
                </div>
                <div className="col-lg-12">
                    <ul className="edl-nav nav nav-pills">
                        <li className="nav-item">
                            <button className="nav-link active" id="pills-1-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-1">{t('All Course.1')}</button>
                        </li>
                        <li className="nav-item">
                            <button className="nav-link" id="pills-2-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-2">Python</button>
                        </li>
                        <li className="nav-item">
                            <button className="nav-link" id="pills-3-tab" data-bs-toggle="pill" data-bs-target="#pills-3">UI
                                Design</button>
                        </li>
                        <li className="nav-item">
                            <button className="nav-link" id="pills-4-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-4">Php</button>
                        </li>
                        <li className="nav-item">
                            <button className="nav-link" id="pills-5-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-5">HTML</button>
                        </li>
                        <li className="nav-item">
                            <button className="nav-link" id="pills-6-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-6">CSS</button>
                        </li>
                    </ul>
                    <div className="tab-content">
                        <div className="tab-pane fade show active" id="pills-1">
                            <div >
                                    <CoursesBox1 />
                                  
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section className="testimonial-courses-area pd-bottom-90">
        <div className="container">
            <div className="row">
                <div className="col-lg-12">
                    <div className="section-title">
                        <h2>{t('People.1')} <i style={{color: 'var(--main-color)'}} className="fa fa-heart"></i></h2>
                    </div>
                </div>
                <div className="col-lg-12">
                    <div className="testimonial-slider owl-carousel">
                        <TestimonialBlock />
                        <TestimonialBlock />
                        <TestimonialBlock />
                        <TestimonialBlock />
                        <TestimonialBlock />
                        <TestimonialBlock />
                        <TestimonialBlock />
                        <TestimonialBlock />
                    </div>
                </div>
            </div>
        </div>
    </section>


    <div className="container mt-4">
{/* Popular Teachers */}
<h3 className="pb-1 mb-4 mt-5">{t('Popular Teachers.1')} <Link to="/popular-teachers" className="float-end btn btn-primary">See All</Link></h3> <div className="row mb-4">
{popularteacherData && popularteacherData.map((teacher, index)=>
<div className="col-md-3">
<div className="card">
<Link to={`/teacher-detail/${teacher.id}`}><img src={teacher.profile_img} className="card-img-top" alt="..." /></Link> <div className="card-body">
<h5 className="card-title"> <Link to={`/teacher-detail/${teacher.id}`}>{teacher.full_name}</Link></h5> </div>
<div className='card-footer'>
<div className="title">
<span>{t('Total Courses:.1')} {teacher.total_teacher_courses}</span>
</div>
</div>
</div>
</div>
)}
</div>
{/*End Popular Teachers */}
{/* Student Testimonial */}
<h3 className="pb-1 mb-4 mt-5">{t('Student Testimonial.1')}</h3>
<div id="carouselExampleIndicators" className="carousel slide bg-dark text-white py-5" data-bs-ride="carousel"> 
<div className="carousel-indicators">
{ testimonialData && testimonialData.map((row, index)=>
<button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to={index} className={index === 0 ? "active": "" }>
</button>
)}
</div>
<div className="carousel-inner">
{testimonialData & testimonialData.map((row,i) =>
<div className={i===0 ? "carousel-item text-center active":"carousel-item text-center" }>
<figure className="text-center">
<blockquote className="blockquote">
<p>{row.reviews}</p>
</blockquote>
<figcaption className="blockquote-footer">
{row.course.title}<cite title="Source Title">{row.student.full_name}</cite>
</figcaption>
</figure>
</div>
)}
</div>
<button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev"> 
<span className="carousel-control-prev-icon" aria-hidden="true"></span>
<span className="visually-hidden">{t('Previous.1')}</span>
</button>
<button className="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next"> 
<span className="carousel-control-next-icon" aria-hidden="true"></span>
<span className="visually-hidden">{t('Next.1')}</span>
</button>
</div>
</div>
</>

)
}
export default Home;
