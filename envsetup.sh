#!/bin/bash

if [ -d "env" ] 
then
    echo "Python virtual environment exists." 
else
    python3 -m venv env
fi
echo '###########test###############'
echo $PWD
echo '###########test###############'
source env/bin/activate


pip3 install -r requirements.txt
cd /var/lib/jenkins/workspace/django_react_pipeline_main/endabackend

python3 manage.py makemigrations
python3 manage.py migrate
echo '################################################test###########################################'
python3 manage.py test
echo '################################################test##########################################'
if [ -d "logs" ] 
then
    echo "Log folder exists." 
else
    mkdir logs
    touch logs/error.log logs/access.log
fi

sudo chmod -R 777 logs
